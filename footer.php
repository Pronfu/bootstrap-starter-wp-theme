<!--
    No references to Javascript files
    use wp_footer() to fire wp_footer action
-->

</div><!-- /.row -->

</div><!-- /.container -->

<!-- Using a widget to display copyright info, check sidebar to see if active, if yes then use dynamic_sidebar to display widget -->
<footer class="blog-footer">
    <?php if ( is_active_sidebar( 'footer_copyright_text' ) ) { dynamic_sidebar( 'footer_copyright_text' ); } ?>
</footer>
<?php wp_footer(); ?>
</body>
</html>