# Bootstrap Starter WordPress Theme
WordPress theme created using Bootstrap v4.0.0-alpha 6 & Normalize.css

![Home Page](https://i.imgur.com/ayiXoe9.png)

![Sample Page](https://i.imgur.com/MHPe4Gj.png)

## Zip Download

https://bitbucket.org/Pronfu/bootstrap-starter-wp-theme/src/master/bootstrapstarter.zip

## Faq

> Do I need a website to get this to work?

* No, you don't. You can run this locally on your computer, or have this just for practice (Find out how to do it thanks to [MakeTechEasier](https://www.maketecheasier.com/setup-local-web-server-all-platforms/) ), but if you wish to show your friends, family and coworkers what you have worked on then you will need to buy a domain and hosting.

> Doesn't this already exist?

* Everything I used to make this already exists but I wanted to put it all together in a way that is useable on older versions of Internet Explorer, easily readable for someone to edit, and easy for someone who is just learning to be able to look at the code play around with it and learn.

> What is this good for?

* Allowing everyone to edit something that works good, to learn from it and to create something that is their own based on this.

> Can I change this up and use this for __________?

* Yes, you can use this for anything. I just ask that you retain one of the licenses and put it in your project / website / whateveryouaregoingtodo. As always credit is appreciated but not required.

## License

> This project is under Unlicense or Beerware or WTFPL. You can choose to use any of these licenses, credit is appreciated but not required. Please see the license file for more details.