<!-- 
    No references to CSS, going to be using functions.php for that
    No meta tags (should be used with an SEO plugin) or favicon (core WordPress functions)
    No <title>, WordPress will manage that
    language_attributes() to display lanuage
    bloginfo(‘charset’) to display encoding for pages and feeds (set in Settings -> Reading)
    wp_head() any background functions or gathering of CSS / JS is performed, and is crucial for functionality and performance of most plugins
    body_class() gives body element different classes based on the page that is being generated
-->

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Adding things to make website look / work great on older versions of Internet Explorer -->

  <!--[if lt IE 9]>

  <!-- get IE to under HTML5 tags - https://github.com/aFarkas/html5shiv -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>

    <!-- get IE to recognize / support pseudo-selectors and pseudo-classes - http://selectivizr.com/ -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr.js"></script>

    <!-- make IE responsive - https://github.com/scottjehl/Respond -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>

  <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
 <!-- Menu list -->
<div class="blog-masthead">
    <div class="container">
        <?php wp_nav_menu
        ( array
        ( 
          'theme_location' => 'header-menu', 
          'menu_class' => 'blog-nav list-inline' 
        ) 
        ); ?>
    </div>
</div>

<!-- Blog name and description -->
<div class="container">
  <div class="blog-header">
   <h1 class="blog-title"><?php bloginfo( 'name' ); ?></h1>
    <?php $description = get_bloginfo( 'description', 'display' ); ?>
    <?php if($description) { ?><p class="lead blog-description"><?php echo $description ?></p><?php } ?>
</div>

    <div class="row">