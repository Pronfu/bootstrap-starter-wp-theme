<!-- To summarize, this block of code includes the Bootstrap CSS and Javascript files as well as your own theme style.css (created in the previous step). We do this by creating a function that includes or enqueues the files and then hook the function to “wp_enqueue_scripts”. This hook is used to enqueue or include items that are meant to appear on the front end. Despite the name, this hook is used for both scripts (JS) and styles (CSS) -->

<?php

//  allow WordPress to take care of the title tag for you
function bootstrapstarter_wp_setup() 
{
    add_theme_support( 'title-tag' );
}

add_action( 'after_setup_theme', 'bootstrapstarter_wp_setup' );


function bootstrapstarter_enqueue_styles() 
{
    wp_register_style('bootstrap', get_template_directory_uri() . 'maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css' );
    $dependencies = array('bootstrap');
    wp_enqueue_style( 'bootstrapstarter-style', get_stylesheet_uri(), $dependencies ); 
}

function bootstrapstarter_enqueue_scripts() 
{
    wp_enqueue_script( 
    'jquery-cdn', 
    '//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.slim.js',
    array(), 
    '3.1.1', 
    true
);

// Bootstrap v4 requires tether 
wp_enqueue_script( 
    'tether-js', 
    '//cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js',
    array(),
    '1.4.0',
    true
);

// And then update your dependency on your bootstrap script
// to use the CDN jQuery and Tether:
wp_enqueue_script( 
    'bootstrap-js', 
    '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js',
    array( 'jquery-cdn', 'tether-js' ), 
    true
);

} // end enqueue_scripts

add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_scripts' );

// Make menu not static

function bootstrapstarter_register_menu() 
{
    register_nav_menu('header-menu', __( 'Header Menu' ));
}
add_action( 'init', 'bootstrapstarter_register_menu' );


function bootstrapstarter_widgets_init() 
{

    // Register a function called "Footer - Copyright Text"
    register_sidebar( array(
        'name'          => 'Footer - Copyright Text',
        'id'            => 'footer_copyright_text',
        'before_widget' => '<div class="footer_copyright_text">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );

        // Sidebar
        register_sidebar( array(
        'name'          => 'Sidebar - Inset',
        'id'            => 'sidebar-1',
        'before_widget' => '<div class="sidebar-module sidebar-module-inset">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
    
    register_sidebar( array(
        'name'          => 'Sidebar - Default',
        'id'            => 'sidebar-2',
        'before_widget' => '<div class="sidebar-module">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );

}
add_action( 'widgets_init', 'bootstrapstarter_widgets_init' );

?>